# Monitor activity with a NodeJS service and a React web app using websockets
Here you have a demo app to demostrate how to create a monitor service that is able to connect to any web application using web sockets.

This demo is based on: https://gitlab.com/jsilversun/electron-monitor-demo

## Known bugs
iohook (Library used to listen to global events like mouse movement and key press) has 2 bugs for macOS and nodeJS.

The bugs are:

1. The mouse activity will stop being monitored when a key is pressed
2. After you press a key, the keyboard monitor will stop monitoring (It only detects 1 key and nothing else)

This library worked well with electron in macOS, not sure which is the cause of this problem.

Here you can find links to the issues of the library:

- https://github.com/wilix-team/iohook/issues/124
- https://github.com/wilix-team/iohook/issues/163


