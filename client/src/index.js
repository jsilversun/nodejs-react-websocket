import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';

// Setup web socket
const socket = new WebSocket('ws://localhost:1337');

socket.onopen = function(e) {
  console.info('[open] Connection established');
  socket.send('START');
};

socket.onclose = function(event) {
  if (event.wasClean) {
    console.info(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
  } else {
    console.error('[close] Connection died');
  }
};

socket.onerror = function(error) {
  console.error(`[error] ${error.message}`);
};
// End websocket setup
ReactDOM.render(<App socket={socket}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
