import React from 'react';
import './App.css';
import events from './events';

function processEvent(currentState, rawData) {
  const newState = {};
  const { type, data } = JSON.parse(rawData);
  console.log(data);
  if (type === events.SCREENSHOT) {
    newState.image = data;
  } else if (type === events.KEY_DOWN) {
    console.log('KEY EVENT', data);
    newState.key = data;
  } else if (type === events.MOUSE_MOVE) {
    newState.mouse = data;
  } else if (type === events.ACTIVE_WINDOW) {
    newState.processData = [...currentState.processData, data];
  }
  return newState;
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { processData: [] };
  }

  componentDidMount() {
    const self = this;
    const { socket } = this.props;
    socket.onmessage = function(event) {
      console.info(`[message] Data received <- server`);
      const newState = processEvent(self.state, event.data);
      self.setState(newState);
    };
  }

  render() {
    const { processData, image, mouse, key } = this.state;
    let processHeaders = [], mouseHeaders = [], keyHeaders = [];
    if (processData.length) {
      processHeaders = Object.keys(processData[0]);
    }
    if (mouse) {
      mouseHeaders = Object.keys(mouse);
    }
    if (key) {
      keyHeaders = Object.keys(key);
    }
    return (
      <div>
        <h2>Show mouse movement event</h2>
        {mouse && <div className="table-responsive">
          <table className="table mouse-event">
            <tr>
              {mouseHeaders.map(header =>
                <th className="col-xs-2 d-inline-block text-truncate" scope="col">
                  {header}
                </th>)}
            </tr>
            <tr>{Object.values(mouse).map(col =>
              <td className="col-xs-2 d-inline-block text-truncate">
                {col}
              </td>)}
            </tr>
          </table>
        </div>
        }
        {!mouse && <h4>Move the mouse to begin</h4>}
        <h2>Show key press event</h2>
        {key && <div className="table-responsive">
          <table className="table key-event">
            <tr>
              {keyHeaders.map(header =>
                <th className="col-xs-2 d-inline-block text-truncate" scope="col">
                  {header}
                </th>)}
            </tr>
            <tr>{Object.values(key).map(col =>
              <td className="col-xs-2 d-inline-block text-truncate">
                {col.toString()}
              </td>)}
            </tr>
          </table>
        </div>}
        {!key && <h4>Press any key to begin</h4>}
        <h2>Take and show a fullscreen screenshot every 5 seconds</h2>
        {
          image && <img className="screenshot" src={`data:image/png;base64,${image}`}/>
        }
        {!image && <h4>Wait 5 seconds</h4>}
        <h2>Show process active window every 5 seconds</h2>
        {processData.length > 0 && <div className="table-responsive">
          <table className="table process-event">
            <tr>
              {processHeaders.map(header =>
                <th className="col-xs-2 d-inline-block text-truncate" scope="col">
                  {header}
                </th>)}
            </tr>
            {processData.map(row => <tr>
              {Object.values(row).map(col => <td
                className="col-xs-2 d-inline-block text-truncate">{col}</td>)}
            </tr>)}
          </table>
        </div>}
        {!processData.length && <h4>Wait 5 seconds</h4>}
      </div>
    );
  }
}

export default App;
