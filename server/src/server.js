const http = require('http');
const WebSocketServer = require('websocket').server;
const ActivityMonitor = require('./ActivityMonitor');
const events = require('./events');

const clients = [];
const port = 1337;

process.title = '+tracker-service';

function sendDataToClients(type, data) {
  for (let i = 0; i < clients.length; i++) {
    const message = { type, data };
    clients[i].sendUTF(JSON.stringify(message));
  }
}

function setupMonitorListener(monitor) {
  const eventsNames = Object.values(events);
  for (const event of eventsNames) {
    monitor.on(event, (data) => {
      sendDataToClients(event, data);
    });
  }
}

const monitor = new ActivityMonitor();
setupMonitorListener(monitor);

// Web Socket Server setup
const server = http.createServer(function(request, response) {});

server.listen(port, function() {
  console.log(`${new Date()} Server is listening on port ${port}`);
});

const wsServer = new WebSocketServer({
  httpServer: server,
});

wsServer.on('request', function(request) {
  console.log(`${new Date()} Connection from origin ${request.origin}.`);

  const connection = request.accept(null, request.origin);
  const index = clients.push(connection) - 1;
  console.log(`${new Date()} Connection accepted.`);
  monitor.start();

  connection.on('close', function(connection) {
    const date = new Date();
    console.log(`${date} Peer ${connection.remoteAddress} disconnected.`);
    clients.splice(index, 1);
    monitor.stop();
  });
});
