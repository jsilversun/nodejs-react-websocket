const ioHook = require('iohook');
const activeWin = require('active-win');
const screenShot = require('screenshot-desktop');
const EventEmitter = require('events');
const events = require('./events');

class ActivityMonitor extends EventEmitter {

  constructor() {
    super();
    this.isRunning = false;
    this.start = this.start.bind(this);
    this.stop = this.stop.bind(this);
    this.sendScreenshot = this.sendScreenshot.bind(this);
    this.sendActiveWindow = this.sendActiveWindow.bind(this);
    this.setMouseListener = this.setMouseListener.bind(this);
    this.setKeyboardListener = this.setKeyboardListener.bind(this);
  }

  start() {
    this.screenShotSender = setInterval(this.sendScreenshot, 5000);
    this.activeWindowSender = setInterval(this.sendActiveWindow, 5000);
    this.setKeyboardListener();
    this.setMouseListener();
    ioHook.start();
    this.isRunning = true;
  }

  stop() {
    clearInterval(this.screenShotSender);
    clearInterval(this.activeWindowSender);
    ioHook.stop();
    this.isRunning = false;
  };

  async sendScreenshot() {
    try {
      const image = await screenShot({ format: 'png' });
      this.emit(events.SCREENSHOT, image.toString('base64'));
    } catch (e) {
      console.error(e);
    }
  }

  async sendActiveWindow() {
    try {
      const processData = await activeWin();
      const formattedObject = {
        title: processData.title,
        app: processData.owner.name,
        bundleId: processData.owner.bundleId,
      };
      this.emit(events.ACTIVE_WINDOW, formattedObject);
    } catch (e) {
      console.error(e);
    }
  }

  setMouseListener() {
    ioHook.on('mousemove', (event) => {
      console.info(event);
      this.emit(events.MOUSE_MOVE, event);
    });
  }

  setKeyboardListener() {
    ioHook.on('keydown', (event) => {
      console.info(event);
      this.emit(events.KEY_DOWN, event);
    });
  }
}

module.exports = ActivityMonitor;